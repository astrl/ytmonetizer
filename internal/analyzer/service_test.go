package analyzer

import (
	"context"
	"testing"
	"time"

	"github.com/googleapis/gax-go/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/vaardan/ytmonetizer/internal/model"
	"go.uber.org/zap/zaptest"
	pb "google.golang.org/genproto/googleapis/cloud/vision/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	pathToFrames = "../../test-data/"
)

var frames = []model.Frame{
	{
		Second: 1,
		Path:   pathToFrames + "frame_00001.png",
	},
	{
		Second: 2,
		Path:   pathToFrames + "frame_00002.png",
	},
	{
		Second: 3,
		Path:   pathToFrames + "frame_00003.png",
	},
	{
		Second: 4,
		Path:   pathToFrames + "frame_00004.png",
	},
	{
		Second: 5,
		Path:   pathToFrames + "frame_00005.png",
	},
	{
		Second: 6,
		Path:   pathToFrames + "frame_00006.png",
	},
	{
		Second: 7,
		Path:   pathToFrames + "frame_00007.png",
	},
	{
		Second: 8,
		Path:   pathToFrames + "frame_00008.png",
	},
	{
		Second: 9,
		Path:   pathToFrames + "frame_00009.png",
	},
	{
		Second: 10,
		Path:   pathToFrames + "frame_00010.png",
	},
}

func getTestFrames(n int) []model.Frame {
	if n < 1 {
		return nil
	}

	if n > len(frames) {
		return frames
	}

	return frames[:n]
}

type ImageAnalyzerMock struct {
	mock.Mock
}

func (a *ImageAnalyzerMock) DetectSafeSearch(ctx context.Context, img *pb.Image, ictx *pb.ImageContext, _ ...gax.CallOption) (*pb.SafeSearchAnnotation, error) {
	args := a.Called(ctx, img, ictx)

	return args.Get(0).(*pb.SafeSearchAnnotation), args.Error(1)
}

func TestAnnotateFramesAsyncSuccess(t *testing.T) {
	ctx := context.Background()
	nFrames := 10
	likelihood := pb.Likelihood_VERY_UNLIKELY

	visionAPIMock := &ImageAnalyzerMock{}
	defer visionAPIMock.AssertExpectations(t)

	log := zaptest.NewLogger(t).Sugar()

	visionAPIMock.On("DetectSafeSearch", ctx, mock.Anything, mock.Anything).Return(
		&pb.SafeSearchAnnotation{
			Adult:    likelihood,
			Spoof:    likelihood,
			Medical:  likelihood,
			Violence: likelihood,
			Racy:     likelihood,
		}, nil)

	service := NewService(visionAPIMock, log)
	frames, errs := service.AnnotateFramesAsync(ctx, getTestFrames(nFrames), 3)
	assert.Nil(t, errs)
	assert.Len(t, frames, nFrames)

	for i := 0; i < nFrames; i++ {
		assert.Equal(t, frames[i].Second, i+1)
		assert.Equal(t, frames[i].Criteria.Adult, model.LikelihoodVeryUnlikely)
		assert.Equal(t, frames[i].Criteria.Spoof, model.LikelihoodVeryUnlikely)
		assert.Equal(t, frames[i].Criteria.Medical, model.LikelihoodVeryUnlikely)
		assert.Equal(t, frames[i].Criteria.Violence, model.LikelihoodVeryUnlikely)
		assert.Equal(t, frames[i].Criteria.Racy, model.LikelihoodVeryUnlikely)
	}
}

func TestAnnotateFrameQuotaError(t *testing.T) {
	ctx := context.Background()
	nFrames := 3

	visionAPIMock := &ImageAnalyzerMock{}
	defer visionAPIMock.AssertExpectations(t)

	log := zaptest.NewLogger(t).Sugar()

	errMsg := "Resource exhausted"
	visionAPIMock.On("DetectSafeSearch", ctx, mock.Anything, mock.Anything).Return(
		&pb.SafeSearchAnnotation{},
		status.Error(codes.ResourceExhausted, errMsg),
	)

	service := NewService(visionAPIMock, log)
	_, errs := service.AnnotateFramesAsync(ctx, getTestFrames(nFrames), 3)
	assert.Len(t, errs, nFrames)
	for _, err := range errs {
		visionErr := visionAPIError{}
		assert.ErrorAs(t, err, &visionErr)
		assert.Contains(t, visionErr.Error(), errMsg)
	}
}

func TestAnnotateFramesAsyncCancelCtx(t *testing.T) {
	nFrames := 10
	likelihood := pb.Likelihood_VERY_UNLIKELY

	visionAPIMock := &ImageAnalyzerMock{}
	defer visionAPIMock.AssertExpectations(t)

	log := zaptest.NewLogger(t).Sugar()

	// unreasonably short deadline to cancel the context
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Millisecond)
	defer cancel()
	visionAPIMock.On("DetectSafeSearch", ctx, mock.Anything, mock.Anything).Return(
		&pb.SafeSearchAnnotation{
			Adult:    likelihood,
			Spoof:    likelihood,
			Medical:  likelihood,
			Violence: likelihood,
			Racy:     likelihood,
		}, nil)

	service := NewService(visionAPIMock, log)
	_, errs := service.AnnotateFramesAsync(ctx, getTestFrames(nFrames), 3)
	assert.Len(t, errs, 1)
	assert.ErrorIs(t, errs[0], ctx.Err())
}

func TestAnnotateFrameAPIsError(t *testing.T) {
	ctx := context.Background()
	nFrames := 5

	visionAPIMock := &ImageAnalyzerMock{}
	defer visionAPIMock.AssertExpectations(t)

	log := zaptest.NewLogger(t).Sugar()

	errMsg := "API Error"
	visionAPIMock.On("DetectSafeSearch", ctx, mock.Anything, mock.Anything).Return(
		&pb.SafeSearchAnnotation{},
		status.Error(codes.FailedPrecondition, errMsg),
	)

	service := NewService(visionAPIMock, log)
	_, errs := service.AnnotateFramesAsync(ctx, getTestFrames(nFrames), 3)
	assert.Len(t, errs, nFrames)
	for _, err := range errs {
		visionErr := visionAPIError{}
		assert.ErrorAs(t, err, &visionErr)
		assert.Contains(t, visionErr.Error(), errMsg)
	}
}
