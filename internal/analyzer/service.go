package analyzer

import (
	"context"
	"fmt"
	"os"
	"sort"
	"time"

	vision "cloud.google.com/go/vision/apiv1"
	"github.com/googleapis/gax-go/v2"
	"gitlab.com/vaardan/ytmonetizer/internal/helpers"
	"gitlab.com/vaardan/ytmonetizer/internal/model"
	pb "google.golang.org/genproto/googleapis/cloud/vision/v1"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	// maxRetries is the number of api call attempts.
	maxRetries = 2
)

type imageAnalyzer interface {
	DetectSafeSearch(ctx context.Context, img *pb.Image, ictx *pb.ImageContext, opts ...gax.CallOption) (*pb.SafeSearchAnnotation, error)
}

type log interface {
	Debug(args ...interface{})
	Debugf(template string, args ...interface{})
	Warn(args ...interface{})
	Warnf(template string, args ...interface{})
	Error(args ...interface{})
	Errorf(template string, args ...interface{})
}

// Service uses Vision API to annotate frames with safe search criteria likelihood.
type Service struct {
	analyzer imageAnalyzer
	log      log
}

// NewService creates Service.
func NewService(analyzer imageAnalyzer, log log) *Service {
	return &Service{analyzer: analyzer, log: log}
}

// AnnotateFramesAsync asynchronously annotates frames with safe search criteria likelihood.
// workers variable represents number of go routines working sending requests to the API at the same time.
// Quotas and limits: https://cloud.google.com/vision/quotas.
func (s *Service) AnnotateFramesAsync(ctx context.Context, frames []model.Frame, workers int) ([]model.FrameAnalyzed, []error) {
	if len(frames) == 0 {
		return nil, nil
	}

	var errors []error
	var results []model.FrameAnalyzed
	framesCh := make(chan model.Frame)

	//start workers and collect their output channels
	workersChs := make([]<-chan workerResult, 0, workers)
	for i := 1; i <= workers; i++ {
		ch := s.runWorkerAnnotator(ctx, framesCh, i)
		workersChs = append(workersChs, ch)
	}

	// send frames to workers
	go func() {
		// close the frames channel when all frames were sent or context was cancelled
		defer close(framesCh)

		for _, frame := range frames {
			select {
			case <-ctx.Done():
				s.log.Warn("Stopping sending analyze requests to the API")
				errors = append(errors, fmt.Errorf("analysis not complete: frame annotation cancelled"))
				return
			case framesCh <- frame:
			}
		}
	}()

	workersCh := helpers.MergeChannels(workersChs...)
	for result := range workersCh {
		if result.Err != nil {
			errors = append(errors, fmt.Errorf("can't analyze frame: %w", result.Err))
			continue
		}

		results = append(results, result.Value)
	}

	sortResults(results)

	if len(errors) != 0 {
		return results, errors
	}

	return results, nil
}

func (s *Service) runWorkerAnnotator(ctx context.Context, frameCh <-chan model.Frame, workerID int) <-chan workerResult {
	resultCh := make(chan workerResult)

	go func() {
		defer close(resultCh)
		defer s.log.Debugf("worker #%d finished", workerID)

		for {
			select {
			case <-ctx.Done():
				return
			case frame, open := <-frameCh:
				if !open {
					return
				}

				res, err := s.annotateFrame(ctx, frame)
				if err != nil {
					resultCh <- workerResult{Err: err}
					continue
				}
				resultCh <- workerResult{Value: res}
			}
		}
	}()

	return resultCh
}

// annotateFrame makes a call to Vision API to annotate a frame.
func (s *Service) annotateFrame(ctx context.Context, frame model.Frame) (model.FrameAnalyzed, error) {
	imgFile, err := os.Open(frame.Path)
	if err != nil {
		return model.FrameAnalyzed{}, fmt.Errorf("open frame image: %w", err)
	}

	defer func() {
		err := imgFile.Close()
		if err != nil {
			s.log.Errorf("can't close file: %v", err)
		}
	}()

	img, err := vision.NewImageFromReader(imgFile)
	if err != nil {
		return model.FrameAnalyzed{}, fmt.Errorf("convert image to bytes: %w", err)
	}

	var resp *pb.SafeSearchAnnotation
	attempt := 0
	for {
		attempt++
		resp, err = s.analyzer.DetectSafeSearch(ctx, img, nil)
		if err == nil {
			// break attempt cycle if no error
			break
		}

		grpcStatus, _ := status.FromError(err)
		if (grpcStatus.Code() != codes.ResourceExhausted) || (attempt == maxRetries) {
			//returns if it's not the quota error or maximum number of retires is reached
			return model.FrameAnalyzed{}, visionAPIError{Message: err.Error()}
		}

		// if it's quota err wait for a bit
		s.log.Warnf("limit or quota reached for request: %w")
		time.Sleep(time.Second)
	}

	criteria, err := mapToSafeSearchAnnotation(resp)
	if err != nil {
		return model.FrameAnalyzed{}, fmt.Errorf("problems with Vison API response: %w", err)
	}

	return model.FrameAnalyzed{
		Second:   frame.Second,
		Criteria: criteria,
	}, nil
}

func sortResults(results []model.FrameAnalyzed) {
	sort.Slice(results, func(i, j int) bool {
		return results[i].Second < results[j].Second
	})
}
