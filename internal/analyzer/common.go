package analyzer

import (
	"fmt"

	"gitlab.com/vaardan/ytmonetizer/internal/model"
	pb "google.golang.org/genproto/googleapis/cloud/vision/v1"
)

type visionAPIError struct {
	Message string
}

func (e visionAPIError) Error() string {
	return e.Message
}

// workerResult worker analyzer's job result.
type workerResult struct {
	Value model.FrameAnalyzed
	Err   error
}

// mapToSafeSearchAnnotation maps protobuf model to business model.
func mapToSafeSearchAnnotation(s *pb.SafeSearchAnnotation) (model.SafeSearchAnnotation, error) {
	if s == nil {
		return model.SafeSearchAnnotation{}, fmt.Errorf("safe search annotation is empty")
	}

	adult, err := mapToLikelihood(s.Adult)
	if err != nil {
		return model.SafeSearchAnnotation{}, fmt.Errorf("'Adult' value %d: %w", s.Adult, err)
	}

	spoof, err := mapToLikelihood(s.Spoof)
	if err != nil {
		return model.SafeSearchAnnotation{}, fmt.Errorf("'Spoof' value %d: %w", s.Spoof, err)
	}

	medical, err := mapToLikelihood(s.Medical)
	if err != nil {
		return model.SafeSearchAnnotation{}, fmt.Errorf("'Medical' value %d: %w", s.Medical, err)
	}

	violence, err := mapToLikelihood(s.Violence)
	if err != nil {
		return model.SafeSearchAnnotation{}, fmt.Errorf("'Violence' value %d: %w", s.Violence, err)
	}

	racy, err := mapToLikelihood(s.Racy)
	if err != nil {
		return model.SafeSearchAnnotation{}, fmt.Errorf("'Racy' value %d: %w", s.Racy, err)

	}

	return model.SafeSearchAnnotation{
		Adult:    adult,
		Spoof:    spoof,
		Medical:  medical,
		Violence: violence,
		Racy:     racy,
	}, nil
}

// mapToLikelihood maps protobuf likelihood to business model.
func mapToLikelihood(v pb.Likelihood) (model.Likelihood, error) {
	switch v {
	case pb.Likelihood_VERY_UNLIKELY:
		return model.LikelihoodVeryUnlikely, nil
	case pb.Likelihood_UNLIKELY:
		return model.LikelihoodUnlikely, nil
	case pb.Likelihood_POSSIBLE:
		return model.LikelihoodPossible, nil
	case pb.Likelihood_LIKELY:
		return model.LikelihoodLikely, nil
	case pb.Likelihood_VERY_LIKELY:
		return model.LikelihoodVeryLikely, nil
	}

	return model.LikelihoodVeryUnlikely, fmt.Errorf("unknown likelihood value")
}
