package frameextractor

import (
	"bytes"
	"context"
	"fmt"
	"io/fs"
	"io/ioutil"
	"os/exec"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/vaardan/ytmonetizer/internal/model"
)

const (
	numDigits = 5 // can potentially contain frames of 27.7h video (if a frame is being sampled every second)
	ext       = ".png"
	prefix    = "frame_"
)

var (
	operationCancelled = fmt.Errorf("frame extraction cancelled")
)

// Service is a frame extractor.
type Service struct {
}

// NewService creates Service.
func NewService() *Service {
	return &Service{}
}

// ExtractFrames extracts frames of the video.
// Set ds to 30 capture a frame every 30 seconds.
// Similarly, if you want to capture a frame every second set ds to 1.
func (s *Service) ExtractFrames(ctx context.Context, ds int, pathToVideo, extractTo string) ([]model.Frame, error) {
	resultCh := extractFrames(ctx, ds, pathToVideo, extractTo)

	select {
	// deadline exceeded
	case <-ctx.Done():
		<-resultCh // emptying to prevent deadlock of the goroutine
		return nil, operationCancelled

	case res := <-resultCh:
		if res.Error != nil {
			return nil, res.Error
		}
		return res.Value, nil
	}
}

type extractFramesResult struct {
	Value []model.Frame
	Error error
}

func extractFrames(ctx context.Context, ds int, pathToVideo, extractTo string) <-chan extractFramesResult {
	ch := make(chan extractFramesResult)

	go func() {
		defer close(ch)

		errBuf := &bytes.Buffer{}
		fps := fmt.Sprintf("fps=1/%d", ds)
		framePattern := fmt.Sprintf("%s/%s%%0%dd%s", extractTo, prefix, numDigits, ext)

		c := exec.Command(
			"ffmpeg", "-i", pathToVideo,
			"-v", "error",
			"-vf", fps, framePattern,
		)
		c.Stderr = errBuf

		go func() {
			<-ctx.Done()
			if c.Process != nil {
				c.Process.Kill() // killing in case ctx was cancelled prematurely; safe to ignore the error
			}
		}()

		err := c.Start()
		if err != nil {
			ch <- extractFramesResult{Error: fmt.Errorf("can't start frame extraction: %w", err)}
			return
		}

		err = c.Wait()
		//firstly, check the buffer
		extractionErrorMsg := errBuf.String()
		if len(extractionErrorMsg) > 0 {
			ch <- extractFramesResult{Error: fmt.Errorf("can't extract frames: %s", extractionErrorMsg)}
			return
		}

		if err != nil {
			ch <- extractFramesResult{Error: fmt.Errorf("frames extraction: %w", err)}
			return
		}

		frames, err := findExtractedFrames(extractTo, ds)
		if err != nil {
			ch <- extractFramesResult{Error: err}
			return
		}

		ch <- extractFramesResult{
			Value: frames,
			Error: nil,
		}
	}()

	return ch
}

// findExtractedFrames finds frames in the folder and returns them in ascending order.
func findExtractedFrames(extractTo string, ds int) ([]model.Frame, error) {
	files, err := ioutil.ReadDir(extractTo)
	if err != nil {
		return nil, fmt.Errorf("can't read directory: %w", err)
	}

	var frames []model.Frame
	for _, file := range files {
		if isFrame(file) {
			fileName := file.Name()
			number, err := strconv.Atoi(fileName[len(prefix) : len(fileName)-len(ext)])
			if err != nil {
				return nil, fmt.Errorf("detected invalid file in the folder with frames: %s", fileName)
			}

			frames = append(frames, model.Frame{
				Second: number * ds, //frame number multiplied by fps
				Path:   fmt.Sprintf("%s/%s", extractTo, fileName),
			})
		}
	}

	sort.Slice(frames, func(i, j int) bool {
		return frames[i].Second < frames[j].Second
	})

	return frames, nil
}

// isFrame checks that the file in the folder has predefined frame name e.g. frame_00005.png
func isFrame(file fs.FileInfo) bool {
	hasValidPrefix := strings.HasPrefix(file.Name(), prefix)
	hasValidFormat := strings.HasSuffix(file.Name(), ext)
	hasValidLength := len(file.Name()) == len(prefix)+numDigits+len(ext)
	notFolder := !file.IsDir()

	if hasValidPrefix && hasValidFormat && hasValidLength && notFolder {
		return true
	}

	return false
}
