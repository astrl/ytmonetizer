package frameextractor

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

const (
	sampleVideoDurationSec = 7
	pathToVideo            = "../../test-data/video_short.mp4"
	pathToExtract          = "./test-frames"
)

type Suite struct {
	suite.Suite
}

func (s *Suite) SetupSuite() {
	ok := s.FileExists(pathToVideo)
	if !ok {
		s.FailNow("no test video file")
	}
}

func (s *Suite) SetupTest() {
	err := os.Mkdir(pathToExtract, 0777)
	s.NoError(err)
}

func (s *Suite) TearDownTest() {
	err := os.RemoveAll(pathToExtract)
	s.NoError(err)
}

func (s *Suite) TestExtractFrameEverySecond() {
	srv := NewService()
	// set generous deadline
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	frames, err := srv.ExtractFrames(ctx, 1, pathToVideo, pathToExtract)
	s.NoError(err)
	s.Len(frames, sampleVideoDurationSec)

	for i, frame := range frames {
		s.Equal(i+1, frame.Second)
		s.Contains(frame.Path, pathToExtract)
	}
}

func (s *Suite) TestExtractFrameEveryFiveSeconds() {
	srv := NewService()
	ds := 5

	// set generous deadline
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	expectedNumberOfFrames := sampleVideoDurationSec / ds
	frames, err := srv.ExtractFrames(ctx, ds, pathToVideo, pathToExtract)
	s.NoError(err)
	s.Len(frames, expectedNumberOfFrames)

	for i := 0; i < expectedNumberOfFrames; i++ {
		s.Equal(i+1, frames[i].Second)
		s.Contains(frames[i].Path, pathToExtract)
	}
}

func (s *Suite) TestDeadline() {
	srv := NewService()

	// set unreasonably short timeout
	ctx, cancel := context.WithTimeout(context.Background(), time.Microsecond)
	defer cancel()

	frames, err := srv.ExtractFrames(ctx, 1, pathToVideo, pathToExtract)
	s.ErrorIs(err, operationCancelled)
	s.Empty(frames)

}

func (s *Suite) TestBadVideoPath() {
	badVideoPath := "./random.mp3"
	srv := NewService()

	// set generous deadline
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	frames, err := srv.ExtractFrames(ctx, 1, badVideoPath, pathToExtract)
	s.Error(err)
	s.Empty(frames)
}

func TestRun(t *testing.T) {
	suite.Run(t, new(Suite))
}
