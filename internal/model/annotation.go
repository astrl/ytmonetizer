package model

// Likelihood safe search criteria likelihood.
type Likelihood int

// Likelihood values.
const (
	LikelihoodVeryUnlikely Likelihood = iota
	LikelihoodUnlikely
	LikelihoodPossible
	LikelihoodLikely
	LikelihoodVeryLikely
)

// LikelihoodTranslation human-readable values of likelihood.
var LikelihoodTranslation = map[Likelihood]string{
	LikelihoodVeryUnlikely: "Very unlikely",
	LikelihoodUnlikely:     "Unlikely",
	LikelihoodPossible:     "Possible",
	LikelihoodLikely:       "Likely",
	LikelihoodVeryLikely:   "Very Likely",
}

// String returns human-readable value of likelihood.
func (l Likelihood) String() string {
	s, ok := LikelihoodTranslation[l]
	if ok {
		return s
	}

	return "Unknown"
}

// SafeSearchAnnotation safe search scan results.
type SafeSearchAnnotation struct {
	Adult    Likelihood
	Spoof    Likelihood
	Medical  Likelihood
	Violence Likelihood
	Racy     Likelihood
}
