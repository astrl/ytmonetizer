package model

// Frame is an extracted frame.
type Frame struct {
	Second int
	Path   string
}

type FrameAnalyzed struct {
	Second   int
	Criteria SafeSearchAnnotation
}
