package pdfgenerator

import (
	"fmt"
	"strings"

	"github.com/johnfercher/maroto/pkg/color"
)

// IsValidOutputFilePath checks if the provided report output path is valid.
func IsValidOutputFilePath(outputFile string) bool {
	return strings.HasSuffix(outputFile, ".pdf")
}

// getTimestamp converts frame's second into the timestamp of format: '10h. 10m. 10s.'.
func getTimestamp(second int) string {
	if second < 1 {
		return "0h. 0m. 0s."
	}

	minutes := second / 60
	hours := minutes / 60
	seconds := second - (hours * 3600) - (minutes * 60)

	return fmt.Sprintf("%dh. %dm. %ds.", hours, minutes, seconds)
}

// pluralizeIfNeeded decides whether the noun should be plural or singular based on the number of entities.
func pluralizeIfNeeded(singular string, number int) string {
	if number == 1 || number == -1 {
		return singular
	}

	return singular + "s"
}

// pluralizeIfNeeded decides on have/has based on the number of entities.
func haveOrHas(number int) string {
	if number == 1 || number == -1 {
		return "has"
	}

	return "have"
}

func colorBlue() color.Color {
	return color.Color{
		Red:   0,
		Green: 102,
		Blue:  204,
	}
}

func colorLightBlue() color.Color {
	return color.Color{
		Red:   153,
		Green: 204,
		Blue:  255,
	}
}

func colorBlack() color.Color {
	return color.Color{
		Red:   0,
		Green: 0,
		Blue:  0,
	}
}
