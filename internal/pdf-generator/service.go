package pdfgenerator

import (
	"context"
	_ "embed"
	"encoding/base64"
	"fmt"
	"time"

	"github.com/johnfercher/maroto/pkg/consts"
	"github.com/johnfercher/maroto/pkg/pdf"
	"github.com/johnfercher/maroto/pkg/props"
	"gitlab.com/vaardan/ytmonetizer/internal/model"
)

const (
	projectLink = "https://.gitlab.com/vaardan/ytmonetizer"
	fontFamily  = consts.Helvetica
)

//go:embed assets/logo.png
var logoBytes []byte

// Service generates pdf report.
type Service struct {
}

// NewService creates Service.
func NewService() *Service {
	return &Service{}
}

// GenerateReport generates a pdf report for the analyzed frames.
func (s *Service) GenerateReport(ctx context.Context, outputFile string, nTotalFrames int, framesOfInterest []model.FrameAnalyzed) error {
	if !IsValidOutputFilePath(outputFile) {
		return fmt.Errorf("output file extension must be '.pdf', got: %s", outputFile)
	}

	// check for cancelled context
	if ctx.Err() != nil {
		return fmt.Errorf("report generation cancelled")
	}

	nFramesOfInterest := len(framesOfInterest)

	m := pdf.NewMaroto(consts.Portrait, consts.A4)
	m.SetPageMargins(10, 15, 10)

	now := time.Now().UTC()
	base64image := base64.StdEncoding.EncodeToString(logoBytes)
	setHeader(m, base64image, now)
	setAbstract(m, nTotalFrames, nFramesOfInterest)
	setFrameAnalysis(m, framesOfInterest)

	err := m.OutputFileAndClose(outputFile)
	if err != nil {
		return fmt.Errorf("can't save pdf report: %w", err)
	}

	return nil
}

func setFrameAnalysis(m pdf.Maroto, framesOfInterest []model.FrameAnalyzed) {
	if len(framesOfInterest) == 0 {
		return
	}

	tableHeader := []string{"Timestamp", "Adult", "Racy", "Violence", "Medical", "Spoof"}
	var tableBody [][]string

	for _, frame := range framesOfInterest {
		row := []string{
			getTimestamp(frame.Second),
			frame.Criteria.Adult.String(),
			frame.Criteria.Racy.String(),
			frame.Criteria.Violence.String(),
			frame.Criteria.Medical.String(),
			frame.Criteria.Spoof.String(),
		}

		tableBody = append(tableBody, row)
	}

	lightBlue := colorLightBlue()
	m.TableList(tableHeader, tableBody, props.TableList{
		Align: consts.Center,
		ContentProp: props.TableListContent{
			GridSizes: []uint{2, 2, 2, 2, 2, 2},
			Color:     colorBlack(),
			Family:    fontFamily,
		},
		HeaderProp: props.TableListContent{
			GridSizes: []uint{2, 2, 2, 2, 2, 2},
			Family:    fontFamily,
			Style:     consts.Normal,
		},
		AlternatedBackground:   &lightBlue,
		VerticalContentPadding: 10,
	})
}

func setAbstract(m pdf.Maroto, nFrames, nFramesOfInterest int) {
	msgTemplate := "    %d %s %s been sampled from the provided video and analyzed on adult content [Adult], " +
		"modifications to the frame's canonical version to make it appear funny or offensive [Spoof]," +
		" medical content [Medical], violent content [Violence], and sexually suggestive content [Racy]" +
		" (may include skimpy or sheer clothing, strategically covered nudity, lewd or provocative poses," +
		" or close-ups of sensitive body areas). %d %s %s been found to contain imagery that may result in" +
		" demonetization of the video."

	m.Row(15, func() {
		m.Col(12, func() {
			m.Text("Result", props.Text{
				Family: fontFamily,
				Style:  consts.Bold,
				Size:   30,
				Align:  consts.Center,
				Color:  colorBlue(),
			})
		})
	})

	m.Row(60, func() {
		m.Col(12, func() {
			msg := fmt.Sprintf(msgTemplate, nFrames, pluralizeIfNeeded("frame", nFrames), haveOrHas(nFrames),
				nFramesOfInterest, pluralizeIfNeeded("frame", nFramesOfInterest), haveOrHas(nFramesOfInterest))

			m.Text(msg, props.Text{
				Family:          fontFamily,
				Size:            14,
				VerticalPadding: 2,
			})
		})
	})
}

// setHeader sets pdf header.
func setHeader(m pdf.Maroto, base64Logo string, now time.Time) {
	m.RegisterHeader(func() {
		m.Row(25, func() {
			m.Col(3, func() {
				_ = m.Base64Image(base64Logo, consts.Png, props.Rect{
					Percent: 80,
				})
			})

			m.Col(7, func() {
				m.Text("Vision Safe Search Report", props.Text{
					Family: fontFamily,
					Style:  consts.Bold,
					Size:   25,
					Align:  consts.Right,
					Color:  colorBlue(),
				})

				m.Text(now.Format(time.RFC822), props.Text{
					Top:    11,
					Family: fontFamily,
					Size:   14,
					Align:  consts.Right,
				})

			})

			m.Col(2, func() {
				m.QrCode(projectLink, props.Rect{
					Left:    10,
					Top:     2,
					Percent: 65,
				})
			})
		})

		m.Line(1.0, props.Line{Color: colorBlack()})
		m.Row(5, func() {})
	})
}
