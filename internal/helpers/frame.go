package helpers

import (
	"gitlab.com/vaardan/ytmonetizer/internal/model"
)

// FilterOutFrames filters out frames that do not pass likelihood threshold.
func FilterOutFrames(frames []model.FrameAnalyzed, threshold model.Likelihood) []model.FrameAnalyzed {
	var filtered []model.FrameAnalyzed

	for _, frame := range frames {
		criteria := []model.Likelihood{
			frame.Criteria.Adult,
			frame.Criteria.Spoof,
			frame.Criteria.Medical,
			frame.Criteria.Violence,
			frame.Criteria.Racy,
		}

		passed := false
		for _, c := range criteria {
			if c >= threshold {
				passed = true
				break
			}
		}

		if passed {
			filtered = append(filtered, frame)

		}
	}
	return filtered
}
