package helpers

import (
	"sync"
)

// MergeChannels merges channels.
func MergeChannels[K any](cs ...<-chan K) <-chan K {
	out := make(chan K)
	var wg sync.WaitGroup
	wg.Add(len(cs))

	for _, c := range cs {
		go func(c <-chan K) {
			for v := range c {
				out <- v
			}
			wg.Done()
		}(c)
	}

	go func() {
		wg.Wait()
		close(out)
	}()

	return out
}
