package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"time"

	vision "cloud.google.com/go/vision/apiv1"
	"gitlab.com/vaardan/ytmonetizer/internal/analyzer"
	frameextractor "gitlab.com/vaardan/ytmonetizer/internal/frame-extractor"
	"gitlab.com/vaardan/ytmonetizer/internal/helpers"
	"gitlab.com/vaardan/ytmonetizer/internal/model"
	pdfgenerator "gitlab.com/vaardan/ytmonetizer/internal/pdf-generator"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const (
	reportPathTemplate = "report_%d.pdf"
	threshold          = model.LikelihoodPossible
)

var closers []func() error

func main() {
	var pathToVideo string
	var captureEveryNSec int
	var nWorkers int
	var devMode bool
	flag.StringVar(&pathToVideo, "v", "", "Path to the video [REQUIRED]")
	flag.IntVar(&captureEveryNSec, "t", 1, "Sample a frame every n seconds. "+
		"If the value is 5 a frame will be sampled for every 5 seconds of the video. Default value is 1")
	flag.IntVar(&nWorkers, "w", 1, "Number of workers that will be sending requests asynchronously to the API. "+
		"Recommended value is 10 to 30. If number is too big you may exceed the quota. Default value is 1")
	flag.BoolVar(&devMode, "dev", false, "Enable development logging. Disabled by default")

	flag.Parse()

	log := mustInitLogger(devMode)
	defer closerCleanup(log)

	if pathToVideo == "" {
		log.Error("No video provided. Usage: --help")
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// also listen for termination signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	go func() {
		<-c
		log.Warn("Aborting...")
		cancel()
	}()

	visionClient, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		log.Error("Can't init Vision API client: %v", err)
		return
	}
	closerAdd(visionClient.Close)

	frameExtractor := frameextractor.NewService()
	imageAnalyzer := analyzer.NewService(visionClient, log)
	pdfGenerator := pdfgenerator.NewService()

	tmpFramesDir, err := os.MkdirTemp("", "frames")
	if err != nil {
		log.Errorf("Can't create temporary folder to extract files to: %v", err)
		return
	}
	closerAdd(func() error {
		return os.RemoveAll(tmpFramesDir)
	})
	log.Debugf("created temp folder for frames at '%s'", tmpFramesDir)

	log.Info("Starting extraction of the frames from the video. Please wait...")
	frames, err := frameExtractor.ExtractFrames(ctx, captureEveryNSec, pathToVideo, tmpFramesDir)
	if err != nil {
		log.Errorf("Problems with the frame extraction: %v", err)
	}

	if len(frames) == 0 {
		log.Warn("No frames were extracted. Exiting...")
		return
	}
	log.Infof("Extracted %d frame(s)", len(frames))

	log.Info("Starting analysis of the extracted frames. Please wait...")
	analyzedFrames, errs := imageAnalyzer.AnnotateFramesAsync(ctx, frames, nWorkers)
	if len(errs) != 0 {
		for _, err := range errs {
			log.Errorf("Vision API problem: %v", err)
		}
		return
	}

	nAnalyzedFrames := len(analyzedFrames)
	framesOfInterest := helpers.FilterOutFrames(analyzedFrames, threshold)

	log.Infof("Out of %d analyzed frame(s) %d are likely to contain NSFW content", nAnalyzedFrames, len(framesOfInterest))

	reportFilePath := fmt.Sprintf(reportPathTemplate, time.Now().Unix())
	err = pdfGenerator.GenerateReport(ctx, reportFilePath, nAnalyzedFrames, framesOfInterest)
	if err != nil {
		log.Errorf("Problem with report creation: %v", err)
		log.Warnf("Dumping raw frames safe search data: %+v", framesOfInterest)
		return
	}
	log.Infof("Generated report file '%s'", reportFilePath)
}

func closerAdd(fn func() error) {
	closers = append(closers, fn)
}

func closerCleanup(log *zap.SugaredLogger) {
	log.Debug("Cleaning up")
	for _, fn := range closers {
		err := fn()
		if err != nil {
			log.Errorf("Clean up: %v", err)
		}
	}
	log.Sync()
}

func mustInitLogger(dev bool) *zap.SugaredLogger {
	var cfg *zap.Config
	if dev {
		cfg = &zap.Config{
			Encoding:    "json",
			Level:       zap.NewAtomicLevelAt(zapcore.DebugLevel),
			OutputPaths: []string{"stdout"},
			Development: true,

			EncoderConfig: zapcore.EncoderConfig{
				MessageKey:    "msg",
				StacktraceKey: "trace",
				LevelKey:      "level",
				EncodeLevel:   zapcore.CapitalLevelEncoder,
				TimeKey:       "tt",
				EncodeTime:    zapcore.RFC3339TimeEncoder,
			},
		}
	} else {
		cfg = &zap.Config{
			Encoding:          "console",
			Level:             zap.NewAtomicLevelAt(zapcore.InfoLevel),
			OutputPaths:       []string{"stdout"},
			Development:       false,
			DisableCaller:     true,
			DisableStacktrace: true,

			EncoderConfig: zapcore.EncoderConfig{
				MessageKey:  "msg",
				LevelKey:    "level",
				EncodeLevel: zapcore.CapitalColorLevelEncoder,
				TimeKey:     "tt",
				EncodeTime:  zapcore.RFC3339TimeEncoder,
			},
		}
	}

	log, err := cfg.Build()
	if err != nil {
		fmt.Printf("can't initialize logger: %v\n", err)
		os.Exit(1)
	}

	return log.Sugar()
}
